<?php

/**
 * StringArray.php
 */
namespace PiecesPHP\Core\DataStructures;

use PiecesPHP\Core\DataStructures\Exceptions\NotAllowedTypeException;

/**
 * StringArray - Representación de un array de strings
 * @category     DataStructures
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2018
 */
class StringArray extends ArrayOf
{

    /**
     * @param mixed $input Un string o array que solo contenga strings
     *
     * @throws NotAllowedTypeException
     */
    public function __construct($input = [])
    {
        parent::__construct($input, self::TYPE_STRING, null);
    }
}
