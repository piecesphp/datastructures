<?php

/**
 * IntegerArray.php
 */
namespace PiecesPHP\Core\DataStructures;

use PiecesPHP\Core\DataStructures\Exceptions\NotAllowedTypeException;

/**
 * IntegerArray - Representación de un array de integers
 * @category     DataStructures
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2018
 */
class IntegerArray extends ArrayOf
{

    /**
     * @param mixed $input Un integer o array que solo contenga integers
     *
     * @throws NotAllowedTypeException
     */
    public function __construct($input = [])
    {
        parent::__construct($input, self::TYPE_INTEGER, null);
    }
}
