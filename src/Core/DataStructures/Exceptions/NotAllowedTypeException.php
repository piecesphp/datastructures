<?php

/**
 * NotAllowedTypeException.php
 */
namespace PiecesPHP\Core\DataStructures\Exceptions;

/**
 * NotAllowedTypeException
 * @category    Exceptions
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2018
 */
class NotAllowedTypeException extends \Exception
{
    /**
     * __construct
     *
     * @param \Throwable $previous
     */
    public function __construct(int $code = 0, \Throwable $previous = null)
    {
        parent::__construct('not_allowed_type_exception', $code, $previous);
    }
}
