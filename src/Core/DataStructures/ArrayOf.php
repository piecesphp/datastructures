<?php

/**
 * ArrayOf.php
 */
namespace PiecesPHP\Core\DataStructures;

use PiecesPHP\Core\DataStructures\Exceptions\MissingQualifiedNameException;
use PiecesPHP\Core\DataStructures\Exceptions\NotAllowedTypeException;

/**
 * ArrayOf - Representación de un array tipado
 * @category    DataStructures
 * @package     PiecesPHP\Core
 * @author      Vicsen Morantes <sir.vamb@gmail.com>
 * @copyright   Copyright (c) 2018
 */
class ArrayOf extends \ArrayObject
{
    /**
     * Constante para establecer el tipo como mixed
     *
     * @var string
     */
    const TYPE_MIXED = 'mixed';

    /**
     * Constante para establecer el tipo como integer
     *
     * @var string
     */
    const TYPE_INTEGER = 'integer';

    /**
     * Constante para establecer el tipo como double
     *
     * @var string
     */
    const TYPE_DECIMAL = 'double';

    /**
     * Constante para establecer el tipo como string
     *
     * @var string
     */
    const TYPE_STRING = 'string';

    /**
     * Constante para establecer el tipo como object
     *
     * Este valor establece que los elementos de entrada sean objetos y requiere
     * que se defina en el constructuctor el nombre cualificado de la clase que adminitirá
     *
     * @var string
     */
    const TYPE_OBJECT = 'object';

    /**
     * Constante para establecer el tipo como self
     *
     * Este valor establece que los elementos de entrada sean instancias de si mismo
     *
     * @var string
     */
    const TYPE_SELF = 'self';

    /**
     * $type
     *
     * @var string
     */
    protected $type = 'string';

    /**
     * $qualifiedName
     *
     * @var string
     */
    protected $qualifiedName = null;

    /**
     * @param mixed $input Elemento(s) de entrada, puede ser un elemento del tipo admitido o un array
     * que contenga elementos únicamente del tipo admitido.
     * @param string $type Tipo de elemento aceptado. Están definidos en la constanstes de clase.
     * Si el TYPE_OBJECT entonces debe ser suministrado el nombre cualificado del objeto.
     * @param string|null $qualifiedName Nombre cualificado del objeto. Puede ser obtenido
     * con la nomenclatura NombreDelObjeto::class
     * @throws NotAllowedTypeException|MissingQualifiedNameException
     */
    public function __construct($input = [], string $type = self::TYPE_MIXED, string $qualifiedName = null)
    {

        $this->validateType($type);

        $this->type = $type;

        if ($this->type === self::TYPE_OBJECT) {
            if ($qualifiedName !== null) {
                $this->qualifiedName = $qualifiedName;
            } else {
                throw new MissingQualifiedNameException();
            }
        }

        if (!is_array($input)) {
            $input = [$input];
        }

        foreach ($input as $item) {
            $this->validateInput($item);
        }

        parent::__construct($input);
    }

    /**
     * @param mixed $value Elemento del tipo admitido
     * @throws NotAllowedTypeException
     * @return void
     */
    public function append($value): void
    {
        $this->validateInput($value);

        parent::append($value);
    }

    /**
     * Intercambia el array por otro
     *
     * @param mixed $input Elemento(s) de entrada, puede ser un elemento del tipo admitido o un array
     * que contenga elementos únicamente del tipo admitido.
     * @return array<mixed> El array anterior
     * @throws NotAllowedTypeException
     */
    public function exchangeArray($input): array
    {
        if (is_array($input)) {
            foreach ($input as $item) {
                $this->validateInput($item);
            }
        } else {
            $this->validateInput($input);
        }
        return parent::exchangeArray($input);
    }

    /**
     * @param string|int|null $index Índice a ser establecido
     * @param mixed $value Elemento de entrada
     * @return void
     * @throws NotAllowedTypeException
     */
    public function offsetSet($index, $value): void
    {
        $this->validateInput($value);

        parent::offsetSet($index, $value);
    }

    /**
     * @param string $type
     * @return void
     * @throws NotAllowedTypeException
     */
    protected function validateType(string $type)
    {
        $valid = false;

        switch ($type) {
            case self::TYPE_INTEGER:
            case self::TYPE_DECIMAL:
            case self::TYPE_STRING:
            case self::TYPE_OBJECT:
            case self::TYPE_SELF:
            case self::TYPE_MIXED:
                $valid = true;
                break;
            default:
                $valid = false;
        }

        if (!$valid) {
            throw new NotAllowedTypeException();
        }
    }

    /**
     * @param mixed $input
     * @throws NotAllowedTypeException
     * @return void
     */
    protected function validateInput($input)
    {
        $valid = false;

        switch ($this->type) {
            case self::TYPE_INTEGER:
                $valid = is_integer($input);
                break;
            case self::TYPE_DECIMAL:
                $valid = is_float($input);
                break;
            case self::TYPE_STRING:
                $valid = is_string($input);
                break;
            case self::TYPE_OBJECT:
                $valid = $input instanceof $this->qualifiedName;
                break;
            case self::TYPE_SELF:
                $class_name = get_class($this);
                $valid = $input instanceof $class_name;
                break;
            case self::TYPE_MIXED:
                $valid = true;
                break;
            default:
                $valid = false;
                break;
        }

        if (!$valid) {
            throw new NotAllowedTypeException();
        }
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getQualifiedName()
    {
        return $this->qualifiedName;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        $elements = $this->getArrayCopy();

        if ($this->type === self::TYPE_OBJECT) {
            $elements = array_map(function ($e) {
                return serialize($e);
            }, $elements);
        }

        $result = @json_encode($elements);

        return is_string($result) ? $result : '';
    }
}
